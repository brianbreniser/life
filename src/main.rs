use std::thread::sleep;
use std::time::Duration;

const WIDTH: usize = 30;
const HEIGHT: usize = 30;
const ERASE_TO_END: &str = "\x1B[K";
const MOVE_CURSOR_TO_PREVIOUS_LINE: &str = "\x1B[F";
const MILLIS: u64 = 10;
const LIFETIME: i32 = 500;

#[derive(Copy, Clone, Debug, PartialEq)]
enum Tile {
    EMPTY,
    ALIVE,
}

// less than 2 dies
// 2 or 3 lives
// more than 3 dies
// empty cell with 3 live neighbors spawns a new one

fn main() {
    let mut array = vec![vec![Tile::EMPTY; WIDTH]; HEIGHT];

    // Debug line
    // array[0][0] = Tile::ALIVE;
    // array[1][1] = Tile::ALIVE;
    // array[2][2] = Tile::ALIVE;
    // array[3][3] = Tile::ALIVE;
    // array[4][4] = Tile::ALIVE;

    // Marker
    // array[3][0] = Tile::ALIVE;

    // initial blinker
    // array[0][0] = Tile::ALIVE;
    // array[0][1] = Tile::ALIVE;
    // array[1][0] = Tile::ALIVE;
    //
    // array[2][3] = Tile::ALIVE;
    // array[3][3] = Tile::ALIVE;
    // array[3][2] = Tile::ALIVE;
    //
    // // Glider
    // // start at 5, 5
    // array[5][5] = Tile::ALIVE;
    // array[5][7] = Tile::ALIVE;
    // array[6][6] = Tile::ALIVE;
    // array[6][7] = Tile::ALIVE;
    // array[7][6] = Tile::ALIVE;

    // array = add_blinker(0, 0, array);
    // array = add_glider(5, 5, array);

    array = randomize();

    // array = make_all_alive(array);

    for i in 1..=LIFETIME {
        clear_board();
        println!("Iteration: {:0>4}/{:0>4}, Speed Of Light: {} millis", i, LIFETIME, MILLIS);
        present(array.clone());
        sleep(Duration::from_millis(MILLIS));
        array = step(array.clone());
    }
}

fn is_even(x: i32) -> bool {
    return x % 2 == 0;
}

fn randomize() -> Vec<Vec<Tile>> {
    let mut new_array = vec![vec![Tile::EMPTY; WIDTH]; HEIGHT];


    for i in new_array.iter_mut() {
        for y in i.iter_mut() {
            match is_even(rand::random::<i32>()) {
                true => *y = Tile::ALIVE,
                false => *y = Tile::EMPTY
            }
        }
    }

    new_array
}

fn add_glider(x: usize, y: usize, array: Vec<Vec<Tile>>) -> Vec<Vec<Tile>> {
    let mut new_array = vec![vec![Tile::EMPTY; WIDTH]; HEIGHT];

    assert!(x >= 0);
    assert!(y >= 0);

    // Copy old
    for (y_index, i) in new_array.iter_mut().enumerate() {
        for (x_index, y) in i.iter_mut().enumerate() {
            *y = array[y_index][x_index];
        }
    }

    // add blinker
    new_array[y + 0][x + 0] = Tile::ALIVE;
    new_array[y + 0][x + 2] = Tile::ALIVE;
    new_array[y + 1][x + 1] = Tile::ALIVE;
    new_array[y + 1][x + 2] = Tile::ALIVE;
    new_array[y + 2][x + 1] = Tile::ALIVE;

    new_array
}

fn add_blinker(x: usize, y: usize, array: Vec<Vec<Tile>>) -> Vec<Vec<Tile>> {
    let mut new_array = vec![vec![Tile::EMPTY; WIDTH]; HEIGHT];

    assert!(x >= 0);
    assert!(y >= 0);

    // Copy old
    for (y_index, i) in new_array.iter_mut().enumerate() {
        for (x_index, y) in i.iter_mut().enumerate() {
            *y = array[y_index][x_index];
        }
    }

    // add blinker
    new_array[y + 0][x + 0] = Tile::ALIVE;
    new_array[y + 0][x + 1] = Tile::ALIVE;
    new_array[y + 1][x + 0] = Tile::ALIVE;

    new_array[y + 2][x + 3] = Tile::ALIVE;
    new_array[y + 3][x + 3] = Tile::ALIVE;
    new_array[y + 3][x + 2] = Tile::ALIVE;

    new_array
}

fn clear_board() {
    for _ in 0..=HEIGHT + 1 {
        print!("{}{}", MOVE_CURSOR_TO_PREVIOUS_LINE, ERASE_TO_END);
    }
}

fn present(array: Vec<Vec<Tile>>) {
    for i in &array {
        print!("[");
        for y in i {
            match y {
                Tile::EMPTY => {
                    // print! {" ."} // debug version
                    print! {"  "}
                }
                Tile::ALIVE => {
                    // print! {"X."} // debug version
                    print! {"X "}
                }
            }
        }
        println!("]");
    }
    println!();
}

fn step(array: Vec<Vec<Tile>>) -> Vec<Vec<Tile>> {
    let mut new_array = vec![vec![Tile::EMPTY; WIDTH]; HEIGHT];

    for (y_index, i) in array.iter().enumerate() {
        for (x_index, y) in i.iter().enumerate() {
            let quad1: i32 =
                if y_index > 0 && x_index > 0 && array[y_index - 1][x_index - 1] == Tile::ALIVE {
                    1
                } else {
                    0
                };
            let quad2: i32 = if y_index > 0 && array[y_index - 1][x_index] == Tile::ALIVE {
                    1
                } else {
                    0
                };
            let quad3: i32 =
                if y_index > 0 && x_index < WIDTH - 1 && array[y_index - 1][x_index + 1] == Tile::ALIVE {
                    1
                } else {
                    0
                };
            let quad4: i32 = if x_index > 0 && array[y_index][x_index - 1] == Tile::ALIVE {
                    1
                } else {
                    0
                };
            let quad6: i32 =
                if x_index < WIDTH - 1 && array[y_index][x_index + 1] == Tile::ALIVE {
                    1
                } else {
                    0
                };
            let quad7: i32 =
                if y_index < HEIGHT - 1 && x_index > 0 && array[y_index + 1][x_index - 1] == Tile::ALIVE {
                    1
                } else {
                    0
                };
            let quad8: i32 =
                if y_index < HEIGHT -1 && array[y_index + 1][x_index] == Tile::ALIVE {
                    1
                } else {
                    0
                };
            let quad9: i32 =
                if y_index < HEIGHT - 1 && x_index < WIDTH - 1 && array[y_index + 1][x_index + 1] == Tile::ALIVE {
                    1
                } else {
                    0
                };

            let total = count_neighbors(quad1, quad2, quad3, quad4,
                                              quad6, quad7, quad8, quad9);

            // println!("x_index: {}, y_index: {}, y: {:?}, count_neighbors: {}", x_index, y_index, y, total);

            match y {
                Tile::ALIVE => {
                    if total > 3 || total < 2 {
                        new_array[y_index][x_index] = Tile::EMPTY;
                    } else {
                        new_array[y_index][x_index] = Tile::ALIVE;
                    }
                }
                Tile::EMPTY => {
                    if total == 3 {
                        new_array[y_index][x_index] = Tile::ALIVE;
                    } else {
                        new_array[y_index][x_index] = Tile::EMPTY;
                    }
                }
            }
        }
    }

    new_array
}

fn count_neighbors(a: i32, b: i32, c: i32, d: i32, e: i32, f: i32, g: i32, h: i32) -> i32 {
    return a + b + c + d + e + f + g + h;
}

// [0, 0]
// [0, 0]
